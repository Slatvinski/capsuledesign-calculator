import { Theme } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import FormControl from "@material-ui/core/FormControl";
import FormLabel from "@material-ui/core/FormLabel";
import { InputProps } from "@material-ui/core/Input";
import InputAdornment from "@material-ui/core/InputAdornment";
import { InputLabelProps } from "@material-ui/core/InputLabel";
import Radio, { RadioProps } from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";
import { inject, observer } from "mobx-react";
import React, { Component } from "react";

const download =
    "https://glcdn.githack.com/denis-bohatyrov/capsuledesign-calculator/raw/master/src/assets/download.png";
import { ApartmentOptions, ApartmentType } from "client/models";
import { UserInfoStore } from "client/stores";
import { withStyles, WithStyles } from "client/ui";

const styles = (theme: Theme) => ({
    area: {
        marginRight: "5%",
        width: "35%",
    },
    areaField: {
        paddingTop: 30,
    },
    button: {
        backgroundColor: "#e63a4a",
        borderRadius: 20,
        color: "white",
        margin: "14px 20px 14px 0",
        padding: "8px 28px",
    },
    buttonHolder: {
        position: "relative",
    },
    container: {
        width: "40%",
    },
    fileInput: {
        height: "100%",
        opacity: 0,
        overflow: "hidden",
        position: "absolute",
        width: "100%",
        // width: "0.1px",
        // height: "0.1px",
        // position: "absolute",
        zIndex: -1,
    },
    form: {
        display: "flex",
        flexDirection: "column",
    },
    icon: {
        height: 23,
        marginRight: 13,
        width: 27,
    },
    label: {
        marginBottom: 25,
        marginTop: 12,
    },
    roomChecked: {
        border: "1px solid",
    },
    roomOption: {
        ["&:first-child"]: {
            fontSize: 11,
        },
        fontFamily: "Open Sans",
        fontSize: 21,
    },
    roomRoot: {
        color: "#0a0a0a",
        height: 44,
        width: 44,
    },
    rooms: {
        display: "flex",
        flexDirection: "row",
    },
    row: {
        alignItems: "center",
        display: "flex",
    },
});

interface IApartmentFormProps extends WithStyles<typeof styles> {
    userInfoStore?: UserInfoStore;
}

@withStyles(styles)
@inject(UserInfoStore.key)
@observer
export class ApartmentFormComponent extends Component<IApartmentFormProps> {
    private fileInput = React.createRef<HTMLInputElement>();

    public render() {
        const { classes, userInfoStore } = this.props;
        const labelProps: InputLabelProps = {
            shrink: true,
        };
        const areaInputProps: InputProps = {
            className: classes.areaField,
            endAdornment: (
                <InputAdornment position="end">
                    <span>м<sup>2</sup></span>
                </InputAdornment>
            ),
        };

        return (
            <div className={classes.container}>
                <Typography variant="title">
                    Ваша квартира
                </Typography>
                <div className={classes.form}>
                    <div className={classes.row}>
                        <TextField
                            id="field"
                            className={classes.area}
                            label="Площадь квартиры"
                            placeholder="28"
                            InputLabelProps={labelProps}
                            InputProps={areaInputProps}
                            margin="normal"
                            type="number"
                            value={userInfoStore.userInfo.area}
                            onChange={this.onAreaChange}
                        />
                        <FormControl component="fieldset" margin="normal">
                            <FormLabel
                                component="label"
                                className={classes.label}
                            >
                                Комнаты
                            </FormLabel>
                                <RadioGroup
                                    aria-label="rooms"
                                    name="rooms"
                                    value={userInfoStore.userInfo.apartment}
                                    className={classes.rooms}
                                    onChange={this.onApartmentChange}
                                >
                                    {ApartmentOptions.map(this.renderRoom)}
                                </RadioGroup>
                        </FormControl>
                    </div>
                    <div className={classes.row}>
                        <div className={classes.buttonHolder}>
                            <Button className={classes.button} onClick={this.onButton}>
                                <img src={download} className={classes.icon} />
                                <Typography color="inherit">
                                    Зарузить планировку
                                </Typography>
                            </Button>
                            <input
                                ref={this.fileInput}
                                type="file"
                                className={classes.fileInput}
                                onChange={this.onFile}
                            />
                        </div>
                        <Typography>(при наличии)</Typography>
                    </div>
                    <TextField
                        id="field"
                        label="Адрес дома или название жилого комплекса:"
                        placeholder="Москва. ул. Арбат, 15"
                        InputLabelProps={labelProps}
                        margin="normal"
                        value={userInfoStore.userInfo.address}
                        onChange={this.onAddressChange}
                    />
                </div>
            </div>
        );
    }

    private onButton = () => {
        this.fileInput.current.click();
    }

    private onFile = (e: React.ChangeEvent<HTMLInputElement>) => {
        const file = e.target.files[0];
        const reader = new FileReader();

        reader.onload = () => {
            this.props.userInfoStore.userInfo.setAreaPlan({
                content: btoa((reader.result as string)),
                name: file.name,
                type: file.type,
            });
        };

        reader.readAsBinaryString(file);
    }

    private renderRoom = (option: { type: ApartmentType, title: string }) => {
        const radioClasses = {
            checked: this.props.classes.roomChecked,
            root: this.props.classes.roomRoot,
        };
        return (
            <Radio
                value={option.type}
                classes={radioClasses}
                className={this.props.classes.roomOption}
                key={`apartment-option-${option.type}`}
                icon={(<span>{option.title}</span>)}
                checkedIcon={(<span>{option.title}</span>)}
            />
        );
    }

    private onApartmentChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const typedStr: keyof typeof ApartmentType = e.target.value as keyof typeof ApartmentType;

        this.props.userInfoStore.userInfo.setApartment(ApartmentType[typedStr]);
    }

    private onAreaChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        this.props.userInfoStore.userInfo.setArea(e.target.value);
    }

    private onAddressChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        this.props.userInfoStore.userInfo.setAddress(e.target.value);
    }
}
