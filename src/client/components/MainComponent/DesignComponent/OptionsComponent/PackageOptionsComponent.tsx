import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import React, { Component } from "react";

import { Capsule, Option, OptionTypes } from "client/models";
import { UserInfoStore } from "client/stores";
import { RadioButtonIcon, withStyles, WithStyles } from "client/ui";

const styles = () => ({
    button: {
        backgroundColor: "#e64a4a",
        borderRadius: 56,
        color: "white",
        margin: "116px 0 74px",
        padding: "16px 50px",
    },
    buttonText: {
        fontSize: 26,
    },
    buttonWrapper: {
        textAlign: "center",
    },
    column: {
        display: "flex",
        flexDirection: "column",
    },
    option: {
        ["&:last-child"]: {
            marginBottom: 0,
        },
        display: "flex",
        marginBottom: 24,
        textTransform: "uppercase",
    },
    radio: RadioButtonIcon.RadioButtonIconStyle,
    radioClass: {
        marginRight: 14,
    },
    row: {
        display: "flex",
        flexDirection: "row",
        marginBottom: 44,
    },
    wrapper: {
        marginRight: 24,
        width: "20%",
    },
});

interface IPackageOptionsProps extends WithStyles<typeof styles> {
    capsule: Capsule;
    options: Option[];
    selected: string[];
    userInfoStore: UserInfoStore;
    onSelect: (val: string) => void;
}

@withStyles(styles)
export class PackageOptionsComponent extends Component<IPackageOptionsProps> {
    public render() {
        const { classes, capsule } = this.props;

        const val = capsule.getSelectedOption(OptionTypes.summary).values[0];

        const text = val === "РЕМОНТ" ? "Заказать дизайн капсулы и ремонт" : "Заказать дизайн капсулы";

        return (
            <div>
                <div className={classes.buttonWrapper}>
                    <Button className={classes.button} onClick={this.onClick}>
                        <Typography color="inherit" className={classes.buttonText}>{text}</Typography>
                    </Button>
                </div>
            </div>
        );
    }

    private onClick = () => {
        const { capsule, userInfoStore } = this.props;
        const url = "https://mandrillapp.com/api/1.0/messages/send.json";
        const body = JSON.stringify({
            key: "rJr75q2wVFHbUunhbSELtQ",
            message: {
                attachments: [userInfoStore.userInfo.areaPlan],
                autotext: true,
                from_email: "baza@capsuledesign.ru",
                html: userInfoStore.userInfo.toString() + capsule.toString(),
                subject: "Заказать капсулу",
                to: [{
                    email: "dkcapsules@gmail.com",
                    type: "to",
                }],
            },
        });
        // @ts-ignore
        fetch(url, { method: "POST", mode: "cors", body }).then((res) => console.debug({ res }));
    }
}
