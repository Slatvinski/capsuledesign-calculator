import FormControl from "@material-ui/core/FormControl";
import Radio from "@material-ui/core/Radio";
import Typography from "@material-ui/core/Typography";
import { inject, observer } from "mobx-react";
import React, { Component } from "react";

const shoplist =
    "https://glcdn.githack.com/denis-bohatyrov/capsuledesign-calculator/raw/master/src/assets/shoplist.svg";
import { Capsule } from "client/models";
import { CapsuleStore } from "client/stores";
import { ContentWrapperComponent, RadioButtonIcon, withStyles, WithStyles } from "client/ui";

const styles = () => ({
    capsule: {
        ["&:after"]: {
            background: "rgba(0, 0, 0, 0.2)",
            content: "' '",
            display: "block",
            height: "100%",
            left: 0,
            position: "absolute",
            right: 0,
            top: 0,
            zIndex: 1,
        },
        position: "relative",
    },
    capsuleContainer: {
        ["&:last-child"]: {
            marginRight: 0,
        },
        cursor: "pointer",
        flex: 1,
        marginRight: 18,
    },
    container: {
        margin: "0 5% 114px",
    },
    heading: {
        backgroundColor: "rgba(0,0,0,0.2)",
        color: "white",
        left: "50%",
        padding: "55px 0",
        position: "absolute",
        textAlign: "center",
        textTransform: "uppercase",
        top: "50%",
        transform: "translate(-50%, -50%)",
        width: "100%",
        zIndex: 2,
    },
    headingText: {
        fontSize: "1.3em",
    },
    image: {
        height: "auto",
        position: "relative",
        width: "100%",
    },
    label: {
        alignItems: "flex-start",
        display: "flex",
        marginTop: 30,
    },
    link: {
        color: "#e64a4a",
    },
    linkText: {
        fontSize: "0.9rem !important",
    },
    radio: RadioButtonIcon.RadioButtonIconStyle,
    shoplist: {
        height: 41,
        margin: "0 10px",
        width: 38,
    },
    wrapper: {
        display: "flex",
        flexDirection: "row",
        justifyContent: "flex-between",
    },
});

interface ICapsulesVariantsProps extends WithStyles<typeof styles> {
    capsuleStore?: CapsuleStore;
}

@withStyles(styles)
@inject(CapsuleStore.key)
@observer
export class CapsulesVariantsComponent extends Component<ICapsulesVariantsProps> {
    public render() {
        return (
            <div className={this.props.classes.container}>
                <ContentWrapperComponent className={this.props.classes.wrapper}>
                    {this.props.capsuleStore.capsules.map(this.renderVariant)}
                </ContentWrapperComponent>
            </div>
        );
    }

    private renderVariant = (capsule: Capsule) => {
        const { capsuleStore, classes } = this.props;

        return (
            <div
                key={`capsule-${capsule.type}`}
                className={classes.capsuleContainer}
            >
                <a href={capsule.link} target="_blank" className={classes.link}>

                    <div className={classes.capsule}>
                        <img src={capsule.image} className={classes.image} />
                        <div className={classes.heading}>
                            <Typography className={classes.headingText} color="inherit">{capsule.title}</Typography>
                        </div>
                    </div>
                </a>
                <div className={classes.label}>
                    <Radio
                        checked={capsuleStore.selectedCapsule.type === capsule.type}
                        classes={{ root: classes.radio }}
                        onClick={this.onClick.bind(this, capsule)}
                        icon={<RadioButtonIcon checked={false} />}
                        checkedIcon={<RadioButtonIcon checked={true} />}
                    />
                    <img src={shoplist} className={classes.shoplist} />
                    <div>
                        <Typography>
                            {capsule.description}
                        </Typography>
                        <a href={capsule.link} target="_blank" className={classes.link}>
                            <Typography variant="caption" color="inherit" className={classes.linkText}>
                                Посмотреть капсулу
                            </Typography>
                        </a>
                    </div>
                </div>
            </div>
        );
    }

    private onClick = (capsule: Capsule) => {
        this.props.capsuleStore.selectCapsule(capsule);
    }
}
