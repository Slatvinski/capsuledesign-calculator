import { action, computed, observable } from "mobx";

export enum ApartmentType {
    studio = "studio",
    one = "one",
    two = "two",
    three = "three",
    four = "four",
    five = "five",
}

export const ApartmentOptions = [
    {
        title: "студия",
        type: ApartmentType.studio,
    },
    {
        title: "1",
        type: ApartmentType.one,
    },
    {
        title: "2",
        type: ApartmentType.two,
    },
    {
        title: "3",
        type: ApartmentType.three,
    },
    {
        title: "4",
        type: ApartmentType.four,
    },
    {
        title: "5",
        type: ApartmentType.five,
    },
];

interface IAreaPlan {
    name: string;
    type: string;
    content: string;
}

export class UserInfo {
    @observable
    private _name: string = "";

    @observable
    private _email: string = "";

    @observable
    private _phone: string = "";

    @observable
    private _area: string = "";

    @observable
    private _apartment: ApartmentType = ApartmentType.studio;

    @observable
    private _plan: string = "";

    @observable
    private _address: string = "";

    @observable
    private _areaPlan: IAreaPlan;

    @computed
    public get name() {
        return this._name;
    }

    @action
    public setName(name: string) {
        this._name = name;
    }

    @computed
    public get email() {
        return this._email;
    }

    @action
    public setEmail(email: string) {
        this._email = email;
    }

    @computed
    public get phone() {
        return this._phone;
    }

    @action
    public setPhone(phone: string) {
        this._phone = phone;
    }

    @computed
    public get area() {
        return this._area;
    }

    @action
    public setArea(area: string) {
        this._area = area;
    }

    @computed
    public get apartment() {
        return this._apartment;
    }

    @action
    public setApartment(apartment: ApartmentType) {
        this._apartment = apartment;
    }

    @computed
    public get plan() {
        return this._plan;
    }

    @action
    public setPlan(plan: string) {
        this._plan = plan;
    }

    @computed
    public get address() {
        return this._address;
    }

    @action
    public setAddress(address: string) {
        this._address = address;
    }

    @computed
    public get areaPlan() {
        return this._areaPlan;
    }

    @action
    public setAreaPlan(plan: IAreaPlan) {
        this._areaPlan = plan;
    }

    public toString() {
        let str = "";

        str = `Имя: ${this._name}, \n`;
        str = `${str}Email: ${this._email}, \n`;
        str = `${str}Телефон: ${this._phone}, \n`;
        str = `${str}Площадь: ${this._area}, \n`;
        str = `${str}Квартирность: ${this._apartment}, \n`;
        str = `${str}Адресс: ${this._address}. \n`;

        return str;
    }
}
