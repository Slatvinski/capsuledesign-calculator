import { action, computed, observable } from "mobx";
import { Option } from "./Option";

export class SelectableOption extends Option {
    private _description: string;
    private _selectables: string[];

    @observable
    private _selected: string;

    constructor(label: string, description: string, selectables: string[]) {
        super(label);

        this._description = description;
        this._selectables = selectables;
        this._selected = selectables[0];
    }

    public get description() {
        return this._description;
    }

    public get selectables() {
        return this._selectables;
    }

    @computed
    public get selected() {
        return this._selected;
    }

    @action
    public setSelected(selected: string) {
        this._selected = selected;
    }

    public toString() {
        return this._selected || "Пусто";
    }
}
