import { Theme } from "@material-ui/core";
import originalWithStyles, {
    ClassNameMap,
    CSSProperties,
    StyleRules,
    WithStylesOptions,
  } from "@material-ui/core/styles/withStyles"; // tslint:disable-line
import { IReactComponent } from "mobx-react";

export { CSSProperties };

export interface IWithStylesForObject<TStyleRulesObject> {
    classes?: Partial<ClassNameMap<Extract<keyof TStyleRulesObject, string>>>;
    theme?: Theme;
}

export type WithStyles<T> = T extends (...args: any[]) => infer U ? IWithStylesForObject<U> : IWithStylesForObject<T>;
  // A much simpler decorator that gives no type errors, at the expense of providing less type information.
type StyleDecorator = <T extends IReactComponent>(clazz: T) => void;

export function withStyles<ClassKey extends string>(
    style: StyleRules<ClassKey>,
    options?: WithStylesOptions,
): StyleDecorator {
// Use the original withStyles function from material-ui, but override the type.
    return originalWithStyles(style, options) as any as StyleDecorator;
}
