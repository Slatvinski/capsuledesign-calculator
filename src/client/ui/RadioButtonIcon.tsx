import classNames from "classnames";
import React, { Component } from "react";

const pointChecked =
    "https://glcdn.githack.com/denis-bohatyrov/capsuledesign-calculator/raw/master/src/assets/point-active.svg";
const point =
    "https://glcdn.githack.com/denis-bohatyrov/capsuledesign-calculator/raw/master/src/assets/point.svg";
import { withStyles, WithStyles } from "./withStyles";

const styles = () => ({
    checked: {
        backgroundImage: `url(${pointChecked}) !important`,
    },
    point: {
        backgroundImage: `url(${point})`,
        height: "100%",
        width: "100%",
    },
});

interface IRadioButtonIconProps extends WithStyles<typeof styles> {
    checked?: boolean;
}

@withStyles(styles)
export class RadioButtonIcon extends Component<IRadioButtonIconProps> {
    public static RadioButtonIconStyle = {
        height: 19,
        width: 19,
    };

    public render() {
        const { checked, classes } = this.props;
        return (
            <div className={classNames("capsule-radio", classes.point, { [classes.checked]: checked })} />
        );
    }
}
