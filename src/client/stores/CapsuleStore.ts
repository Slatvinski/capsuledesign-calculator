import { action, observable } from "mobx";

// tslint:disable: max-line-length

import {
    Capsule,
    CapsuleType,
    DoorOption,
    FloorOption,
    ImageShape,
    IOption,
    Option,
    OptionTypes,
    SelectableOption,
} from "../models";

const amnezia = "https://static.tildacdn.com/tild6262-6565-4932-b936-643066333434/__2018-09-18__135751.png";
const anonimous = "https://static.tildacdn.com/tild3535-6361-4162-a266-333265383165/__2018-09-18__135733.png";
const marshmallow = "https://static.tildacdn.com/tild3461-3833-4139-b635-366634643130/__2018-09-18__135805.png";
const transformer = "https://static.tildacdn.com/tild3565-3561-4435-a264-336436326231/__2018-08-15__104817.png";

const door =
    "https://glcdn.githack.com/denis-bohatyrov/capsuledesign-calculator/raw/master/src/assets/doors/door-1.png";
const door2 =
    "https://glcdn.githack.com/denis-bohatyrov/capsuledesign-calculator/raw/master/src/assets/doors/door-2.png";

const anonimousLaminate =
    "https://glcdn.githack.com/denis-bohatyrov/capsuledesign-calculator/raw/master/src/assets/floors/anonimous/laminate.png";
const anonimousPlateOne =
    "https://glcdn.githack.com/denis-bohatyrov/capsuledesign-calculator/raw/master/src/assets/floors/anonimous/plate-1.png";
const anonimousPlateTwo =
    "https://glcdn.githack.com/denis-bohatyrov/capsuledesign-calculator/raw/master/src/assets/floors/anonimous/plate-2.png";
const anonimousPlateThree =
    "https://glcdn.githack.com/denis-bohatyrov/capsuledesign-calculator/raw/master/src/assets/floors/anonimous/plate-3.png";
const anonimousPlateFour =
    "https://glcdn.githack.com/denis-bohatyrov/capsuledesign-calculator/raw/master/src/assets/floors/anonimous/plate-4.png";

const amneziaLaminate =
    "https://glcdn.githack.com/denis-bohatyrov/capsuledesign-calculator/raw/master/src/assets/floors/amnezia/laminate.png";
const amneziaPlateOne =
    "https://glcdn.githack.com/denis-bohatyrov/capsuledesign-calculator/raw/master/src/assets/floors/amnezia/plate-1.png";
const amneziaPlateTwo =
    "https://glcdn.githack.com/denis-bohatyrov/capsuledesign-calculator/raw/master/src/assets/floors/amnezia/plate-2.png";
const amneziaPlateThree =
    "https://glcdn.githack.com/denis-bohatyrov/capsuledesign-calculator/raw/master/src/assets/floors/amnezia/plate-3.png";

const marshmallowLaminate = "https://glcdn.githack.com/denis-bohatyrov/capsuledesign-calculator/raw/master/src/assets/floors/marshmallow/laminate.png";
const marshmallowPlateOne = "https://glcdn.githack.com/denis-bohatyrov/capsuledesign-calculator/raw/master/src/assets/floors/marshmallow/plate-1.png";
const marshmallowPlateTwo = "https://glcdn.githack.com/denis-bohatyrov/capsuledesign-calculator/raw/master/src/assets/floors/marshmallow/plate-2.png";
const marshmallowPlateThree = "https://glcdn.githack.com/denis-bohatyrov/capsuledesign-calculator/raw/master/src/assets/floors/marshmallow/plate-3.png";

const transformerLaminate = "https://glcdn.githack.com/denis-bohatyrov/capsuledesign-calculator/raw/master/src/assets/floors/transformer/laminate.png";
const transformerPlateOne = "https://glcdn.githack.com/denis-bohatyrov/capsuledesign-calculator/raw/master/src/assets/floors/transformer/plate-1.png";
const transformerPlateTwo = "https://glcdn.githack.com/denis-bohatyrov/capsuledesign-calculator/raw/master/src/assets/floors/transformer/plate-2.png";
const transformerPlateThree = "https://glcdn.githack.com/denis-bohatyrov/capsuledesign-calculator/raw/master/src/assets/floors/transformer/plate-3.png";

const AnoninousOptions: IOption[] = [
    {
        options: [
            new SelectableOption(
                "Гладкие окрашеные стены",
                "Матовая водно-дисперсионная краска. Англия. " +
                "Эти краски отличаются исключительно красивыми, насыщенными и глубокими " +
                "цветами. Они экологически чистые, так как в производстве используются натуральные " +
                "красители, льняное масло, известь, мел, каолин.Отвечают самым строгим стандартам " +
                "ЕС в области защиты окружающей среды.",
                [
                    "#8f8f8d",
                    "#c9c8c6",
                ],
            ),
        ],
        type: OptionTypes.wall,
    },
    {
        options: [
            new FloorOption(
                "Ламинат",
                [
                    {
                        description: "32-33 класс, Россия",
                        img: anonimousLaminate,
                        shape: ImageShape.circle,
                    },
                ],
            ),
            new FloorOption(
                "Плитка в прихожую",
                [
                    {
                        description: "Керамогранит под дерево, текстура натуральный дуб, Россия",
                        img: anonimousPlateOne,
                        shape: ImageShape.circle,
                    },
                ],
            ),
            new FloorOption(
                "Плитка на кухню",
                [
                    {
                        description: "Плитка керамогранит под мрамор калаката. Размер 60х60, фартук для кухни.",
                        img: [anonimousPlateTwo, anonimousPlateOne],
                        shape: ImageShape.circle,
                    },
                ],
            ),
            new FloorOption(
                "Плитка в санузел",
                [
                    {
                        description: "Белая глянцевая плитка «кабанчик», 7х15 с фаской. Россия.",
                        img: anonimousPlateThree,
                        shape: ImageShape.rect,
                    },
                    {
                        description: "Керамогранит с графичным черно-белым орнаментом, форма октагон.",
                        img: anonimousPlateFour,
                        shape: ImageShape.oct,
                    },
                ],
            ),
        ],
        type: OptionTypes.floor,
    },
    {
        options: [
            new SelectableOption(
                "Натяжной",
                "Матовый, белого цвета, производство Германия.",
                [
                    "#f6f6f3",
                ],
            ),
        ],
        type: OptionTypes.ceil,
    },
    {
        options: [
            new DoorOption(
                "Современные",
                "Гладкие, окрашенная эмаль. Россия.",
                [
                    "#f6f6f3",
                ],
                door,
            ),
        ],
        type: OptionTypes.door,
    },
    {
        options: [
            new SelectableOption(
                "Европласт",
                "Гладкая полиуретановая лепнина, размер 10см.",
                [
                    "#f6f6f3",
                ],
            ),
        ],
        type: OptionTypes.baseboard,
    },
    {
        options: [
            new SelectableOption(
                "Накладные глянцевые",
                "Legrand Quteo, Россия",
                [],
            ),
        ],
        type: OptionTypes.power,
    },
    {
        options: [
            new Option("Проект дизайн"),
            new Option("Ремонт"),
        ],
        type: OptionTypes.summary,
    },
    {
        options: [
            new Option("Замена радиаторов"),
            new Option("Замена окон"),
            new Option("Гипсокартонный потолок вместо натяжного"),
            new Option("Замена входной двери"),
            new Option("Шумоизоляция"),
            new Option("Кондиционирование"),
        ],
        type: OptionTypes.additional,
    },
    {
        options: [
            new Option("Подбор мягкой мебели"),
            new Option("Подбор текстиля и декора"),
            new Option("Подбор декоративного света"),
            new Option("Подбор текстиля и декора"),
        ],
        type: OptionTypes.package,
    },
];

const AmneziaOptions: IOption[] = [
    {
        options: [
            new SelectableOption(
                "Гладкие окрашеные стены",
                "Матовая водно-дисперсионная краска. Англия. " +
                "Эти краски отличаются исключительно красивыми, насыщенными и глубокими " +
                "цветами. Они экологически чистые, так как в производстве используются натуральные " +
                "красители, льняное масло, известь, мел, каолин.Отвечают самым строгим стандартам " +
                "ЕС в области защиты окружающей среды.",
                [
                    "#72283b",
                    "#91412f",
                    "#bda89c",
                    "#eae4d8",
                ],
            ),
        ],
        type: OptionTypes.wall,
    },
    {
        options: [
            new FloorOption(
                "Ламинат",
                [
                    {
                        description: "32-33 класс, Россия \n Текстура - натуральный ясень.",
                        img: amneziaLaminate,
                        shape: ImageShape.circle,
                    },
                ],
            ),
            new FloorOption(
                "Плитка в санузел",
                [
                    {
                        description: "Керамогранит под дерево, текстура натуральный дуб, Россия",
                        img: amneziaPlateOne,
                        shape: ImageShape.circle,
                    },
                    {
                        description: "Керамогранит с графичным орнаментом, форма октагон.",
                        img: amneziaPlateThree,
                        shape: ImageShape.circle,
                    },
                ],
            ),
            new FloorOption(
                "Плитка на кухню",
                [
                    {
                        description: "Фартук и пол кухня керамогранит 60х60, полуполированный, Россия.",
                        img: amneziaPlateTwo,
                        shape: ImageShape.circle,
                    },
                ],
            ),
        ],
        type: OptionTypes.floor,
    },
    {
        options: [
            new SelectableOption(
                "Натяжной",
                "Матовый, белого цвета, производство Германия.",
                [
                    "#f6f6f3",
                ],
            ),
        ],
        type: OptionTypes.ceil,
    },
    {
        options: [
            new DoorOption(
                "Современные",
                "Гладкие, окрашенная эмаль. Россия.",
                [
                    "#f6f6f3",
                ],
                door,
            ),
        ],
        type: OptionTypes.door,
    },
    {
        options: [
            new SelectableOption(
                "Европласт",
                "Гладкая полиуретановая лепнина, размер 10см.",
                [
                    "#f6f6f3",
                ],
            ),
        ],
        type: OptionTypes.baseboard,
    },
    {
        options: [
            new Option("Проект дизайн"),
            new Option("Ремонт"),
        ],
        type: OptionTypes.summary,
    },
    {
        options: [
            new Option("Замена радиаторов"),
            new Option("Замена окон"),
            new Option("Гипсокартонный потолок вместо натяжного"),
            new Option("Замена входной двери"),
            new Option("Шумоизоляция"),
            new Option("Кондиционирование"),
        ],
        type: OptionTypes.additional,
    },
    {
        options: [
            new Option("Подбор мягкой мебели"),
            new Option("Подбор текстиля и декора"),
            new Option("Подбор декоративного света"),
            new Option("Подбор текстиля и декора"),
        ],
        type: OptionTypes.package,
    },
];

const MarshmallowOptions: IOption[] = [
    {
        options: [
            new SelectableOption(
                "Гладкие окрашеные стены",
                "Матовая водно-дисперсионная краска. Англия. " +
                "Эти краски отличаются исключительно красивыми, насыщенными и глубокими " +
                "цветами. Они экологически чистые, так как в производстве используются натуральные " +
                "красители, льняное масло, известь, мел, каолин.Отвечают самым строгим стандартам " +
                "ЕС в области защиты окружающей среды.",
                [
                    "#ddc3ba",
                    "#0a324a",
                    "#e2e3df",
                ],
            ),
        ],
        type: OptionTypes.wall,
    },
    {
        options: [
            new FloorOption(
                "Ламинат",
                [
                    {
                        description: "32-33 класс, Россия \n Текстура - благородный орех.",
                        img: marshmallowLaminate,
                        shape: ImageShape.circle,
                    },
                ],
            ),
            new FloorOption(
                "Плитка в санузел",
                [
                    {
                        description: "Белая глянцевая плитка \"кабанчик\", 7х15 с фаской. Россия",
                        img: marshmallowPlateOne,
                        shape: ImageShape.rect,
                    },
                    {
                        description: "Серая глянцевая плитка \"кабанчик\", 7х15 с фаской. Россия",
                        img: marshmallowPlateTwo,
                        shape: ImageShape.rect,
                    },
                    {
                        description: "Полы - плитка под дерево, текстура натуральный дуб, Россия.",
                        img: marshmallowPlateThree,
                        shape: ImageShape.rect,
                    },
                ],
            ),
            new FloorOption(
                "Плитка на кухню",
                [
                    {
                        description: "Фартук - белая глянцевая плитка \"кабанчик\", 7х15 с фаской. Россия",
                        img: marshmallowPlateOne,
                        shape: ImageShape.rect,
                    },
                    {
                        description: "Полы - плитка под дерево, текстура натуральный дуб, Россия.",
                        img: marshmallowPlateThree,
                        shape: ImageShape.rect,
                    },
                ],
            ),
        ],
        type: OptionTypes.floor,
    },
    {
        options: [
            new SelectableOption(
                "Натяжной",
                "Матовый, белого цвета, производство Германия.",
                [
                    "#f6f6f3",
                ],
            ),
        ],
        type: OptionTypes.ceil,
    },
    {
        options: [
            new DoorOption(
                "Классические",
                "Двери «Волховец» из массива сосны со скрытыми петлями и бесшумными " +
                    "магнитными замками, с двойной или одинарной филенкой, Россия. Отделка " +
                    " — натуральный шпон, крашенные в белый цвет. Плинтус в цвет стен и гладкий карниз.",
                [
                    "#f6f6f3",
                ],
                door2,
            ),
        ],
        type: OptionTypes.door,
    },
    {
        options: [
            new SelectableOption(
                "Европласт",
                "Гладкая полиуретановая лепнина, размер 10см.",
                [
                    "#f6f6f3",
                ],
            ),
            new SelectableOption(
                "Молдинги",
                "Стеновые тонкие молдинги 5см.",
                [
                    "#f6f6f3",
                ],
            ),
        ],
        type: OptionTypes.baseboard,
    },
    {
        options: [
            new SelectableOption(
                "Накладные глянцевые",
                "Legrand Quteo, Россия",
                [],
            ),
        ],
        type: OptionTypes.power,
    },
    {
        options: [
            new Option("Проект дизайн"),
            new Option("Ремонт"),
        ],
        type: OptionTypes.summary,
    },
    {
        options: [
            new Option("Замена радиаторов"),
            new Option("Замена окон"),
            new Option("Гипсокартонный потолок вместо натяжного"),
            new Option("Замена входной двери"),
            new Option("Шумоизоляция"),
            new Option("Кондиционирование"),
        ],
        type: OptionTypes.additional,
    },
    {
        options: [
            new Option("Подбор мягкой мебели"),
            new Option("Подбор текстиля и декора"),
            new Option("Подбор декоративного света"),
            new Option("Подбор текстиля и декора"),
        ],
        type: OptionTypes.package,
    },
];

const TransformerOptions: IOption[] = [
    {
        options: [
            new SelectableOption(
                "Гладкие окрашеные стены (оттенок цвета на выбор - бежевый или серый)",
                "Матовая водно-дисперсионная краска. Англия. " +
                "Эти краски отличаются исключительно красивыми, насыщенными и глубокими " +
                "цветами. Они экологически чистые, так как в производстве используются натуральные " +
                "красители, льняное масло, известь, мел, каолин.Отвечают самым строгим стандартам " +
                "ЕС в области защиты окружающей среды.",
                [
                    "#d4c3b3",
                    "#e2e3e0",
                ],
            ),
        ],
        type: OptionTypes.wall,
    },
    {
        options: [
            new FloorOption(
                "Ламинат",
                [
                    {
                        description: "32-33 класс, Россия \n Текстура - натуральный ясень.",
                        img: transformerLaminate,
                        shape: ImageShape.circle,
                    },
                ],
            ),
            new FloorOption(
                "Плитка в санузел",
                [
                    {
                        description: "Керамогранит под дерево, текстура натуральный дуб, Россия",
                        img: transformerPlateOne,
                        shape: ImageShape.circle,
                    },
                    {
                        description: "Керамогранит матовый под бетон, Россия.",
                        img: transformerPlateTwo,
                        shape: ImageShape.circle,
                    },
                    {
                        description: "Фартук и пол кухня керамогранит 60х60, полуполированный, Россия.",
                        img: amneziaPlateTwo,
                        shape: ImageShape.circle,
                    },
                ],
            ),
        ],
        type: OptionTypes.floor,
    },
    {
        options: [
            new SelectableOption(
                "Натяжной",
                "Матовый, белого цвета, производство Германия.",
                [
                    "#f6f6f3",
                ],
            ),
        ],
        type: OptionTypes.ceil,
    },
    {
        options: [
            new DoorOption(
                "Современные",
                "Гладкие, окрашенная эмаль. Россия.",
                [
                    "#f6f6f3",
                ],
                door,
            ),
        ],
        type: OptionTypes.door,
    },
    {
        options: [
            new Option("Проект дизайн"),
            new Option("Ремонт"),
        ],
        type: OptionTypes.summary,
    },
    {
        options: [
            new Option("Замена радиаторов"),
            new Option("Замена окон"),
            new Option("Гипсокартонный потолок вместо натяжного"),
            new Option("Замена входной двери"),
            new Option("Шумоизоляция"),
            new Option("Кондиционирование"),
        ],
        type: OptionTypes.additional,
    },
    {
        options: [
            new Option("Подбор мягкой мебели"),
            new Option("Подбор текстиля и декора"),
            new Option("Подбор декоративного света"),
        ],
        type: OptionTypes.package,
    },
];

export class CapsuleStore {
    public static key = "capsuleStore";

    public capsules: Capsule[];

    @observable
    public selectedCapsule: Capsule;

    constructor() {
        this.capsules = [
            new Capsule(
                "Anonimous",
                CapsuleType.anonimous,
                "Капсульный Интерьер. База - ANONIMOUS",
                anonimous,
                "http://capsuledesign.ru/baza1",
                AnoninousOptions,
            ),
            new Capsule(
                "Amnezia",
                CapsuleType.amnezia,
                "Капсульный Интерьер. База - AMNEZIA",
                amnezia,
                "http://capsuledesign.ru/baza2",
                AmneziaOptions,
            ),
            new Capsule(
                "Marshmallow",
                CapsuleType.marshmallow,
                "Капсульный Интерьер. База - MARSHMALLOW",
                marshmallow,
                "http://capsuledesign.ru/baza3",
                MarshmallowOptions,
            ),
            new Capsule(
                "Трансформер",
                CapsuleType.transformer,
                "Капсула Трансформер: для малогабаритных квартир",
                transformer,
                "http://capsuledesign.ru/transformer",
                TransformerOptions,
            ),
        ];

        this.selectedCapsule = this.capsules[0];
    }

    @action
    public selectCapsule(capsule: Capsule) {
        this.selectedCapsule = capsule;
    }
}
