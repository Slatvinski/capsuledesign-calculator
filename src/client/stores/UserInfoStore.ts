import { action, observable } from "mobx";

import { UserInfo } from "../models";

export class UserInfoStore {
    public static key = "userInfoStore";

    @observable
    public userInfo: UserInfo;

    constructor() {
        this.userInfo = new UserInfo();
    }
}
